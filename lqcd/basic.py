from collections import defaultdict
import numpy as np

def rec_dd():
    return defaultdict(rec_dd)

def set_scale(a):
    #using NIST data for hbar and c
    #hbar[eV] 6.582 119 514 x 10-16 eV s
    #speed of light [m/2] 299 792 458 m s-1
    #spacing must be in fermi
    hbarc=197.32697879518255
    scale=hbarc/a
    return scale

def list_to_string(mm):
  return str('{0:d}{1:d}{2:d}').format(mm[0],mm[1],mm[2]).strip()


def string_to_list(smom):
  strlen = len(smom)
  tmplist=[]
  for i in range(strlen):
    tmplist.append(smom[i])
  
  tmplistlen=len(tmplist)
  if '-' in tmplist:
    negsign_loc = [minus_loc for minus_loc in range(tmplistlen) if '-'==tmplist[minus_loc]]
  
    for i in negsign_loc:
      tmp = int(tmplist[i]+tmplist[i+1])
      tmplist[i]=tmp

    droplocs=[i+1 for i in negsign_loc]
  
    reducedlist = np.delete(tmplist,droplocs).tolist()
    out=[]
    for i in range(len(reducedlist)):
      out.append(int(reducedlist[i]))
  else:
    out=[]
    for i in range(strlen):
      out.append(int(smom[i]))

  return out

